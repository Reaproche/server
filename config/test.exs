import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :reaproche, ReaprocheWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "3bnBv5lOB4OsLDZpeJt0HlEabq7sP1Y7yr5kgBj/9mto2ZQm4iPTGVwmWWRoKrIA",
  server: false

# In test we don't send emails.
config :reaproche, Reaproche.Mailer,
  adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

defmodule ReaprocheWeb.PageController do
  use ReaprocheWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
